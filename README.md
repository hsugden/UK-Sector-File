# UK Sector File Project

The UK (EGTT and EGPX FIRs) Sector File project is an open source collaborative project to provide the highest quality sector file to VATSIM UK controllers.

A new sector file will be released in line with each AIRAC cycle (as long as significant changes warrant such as release).

If you wish to contribute, [take a look at our contribution guide](https://gitlab.com/vatsim-uk/UK-Sector-File/blob/master/Contributing.md).

Note: past AIRAC amendments are stored [here](https://drive.google.com/folderview?id=0B-vBWgjwDAzqcThza1lIaHJDbEU&usp=sharing).